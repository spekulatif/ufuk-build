//
//  AppDelegate.h
//  Build
//
//  Created by Ufuk on 7/21/15.
//  Copyright (c) 2015 Ufuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

