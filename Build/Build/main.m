//
//  main.m
//  Build
//
//  Created by Ufuk on 7/21/15.
//  Copyright (c) 2015 Ufuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
